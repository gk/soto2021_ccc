# CCC 2021 Slides

This repository contains the slides for the CCC 2021 presentation on Tor
named "State of the Onion". The talk will be presented by ggus and GeKo.

## Building the slides

1. Update the submodules to fetch the `onion-tex` dependency: `git submodule
   update --init --recursive`.

2. Run `make`.
